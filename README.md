Prima pagina 'Home' prezinta tipul evenimentului si recenziile date de clienti

    * In functie de evenimentul ales se poate rezerva locul dat de: 
              - nume si prenume
              - numar telefon
              - mail 
              - data
              - locatie
              - tipul serviciului

Despre mine - pagina configurabila, foloseste antetul din pagina de pornire, prezinta o scurta descriere despre fotograf

Agenda - Intr-o baza de date se retine fiecare data ocupata (ID, Nume si prenume, Numar telefon, Data, Locatie, Serviciu, Actions: DELETE si UPDATE). Agenda trebuie sa fie vizibila doar daca superuser-ul este logat si va fi ordonata by date. 

Contact - Prezinta un formular de contact cu adresa fotografului, numarul de telefon si mail 

Serviciile - modificabile din baza de date (se descriu pachetele oferite)

Termeni&Conditii - se memoreaza o pagina HTML statica in templates(functionarea ei data de titlu-paragraf)

-- panel de login (url ascuns 'administrare ' ) daca site-ul se vinde altor fotografi sa isi poata configura serviciile,portofoliul, etc..