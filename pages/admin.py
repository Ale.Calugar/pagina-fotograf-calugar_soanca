from django.contrib import admin
from .models import Client, Serviciu


# Register your models here.

admin.site.register(Client)
admin.site.register(Serviciu)
