from django.core.mail import send_mail, BadHeaderError
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from .models import Client, Serviciu
from .forms import ContactForm

def home_page_view(request):
    return render(request, 'home.html')

def aboutme(request):
    return render(request, 'aboutme.html')

def contact(request):
    return render(request, 'contact.html')

def nunta(request):
    return render(request, 'nunta.html')

def botez(request):
    return render(request, 'botez.html')

def majorat(request):
    return render(request, 'majorat.html')

def sedinta(request):
    return render(request, 'sedinta.html')

def conditii(request):
    return render(request, 'conditii.html')

def portofoliu(request):
    return render(request, 'portofoliu.html')

class AboutView(TemplateView):
    template_name = 'servicii.html'

class ClientListView(ListView):
    model = Client
    template_name = 'clienti.html'
    context_object_name = 'clienti'

class ClientDetailView(DetailView):
    model = Client
    template_name ='detail.html'
    context_object_name = 'client'
    pk_url_kwarg = 'id'

class ClientCreateView(LoginRequiredMixin, CreateView):
    model = Client
    template_name ='create.html'
    fields = '__all__'
    success_url = reverse_lazy('clienti')
    def post(self, request, *args, **kwargs):
        form=self.get_form()
        if form.is_valid():
            rezervari=Client.objects.filter(data=form.cleaned_data["data"])
            if len(rezervari) == 0:
                form.save()
                flag=True
                return render(request, 'clienti.html', {'flag':flag})
            else:
                return render(request, 'create.html', {'form': form, 'flag': True})

class ClientUpdateView(PermissionRequiredMixin,UpdateView):
    model = Client
    permission_required=('pages.can_edit')
    template_name = 'update.html'
    context_object_name = 'client'
    fields = '__all__'
    success_url = reverse_lazy('clienti')
    pk_url_kwarg = 'id'

class ClientDeleteView(PermissionRequiredMixin, DeleteView):
    model = Client
    permission_required = ('pages.can_delete')
    template_name = 'delete.html'
    context_object_name = 'client'
    fields = '__all__'
    success_url = reverse_lazy('clienti')
    pk_url_kwarg = 'id'

class ServiciuListView(ListView):
    model=Serviciu
    template_name = 'services.html'
    context_object_name = 'services'

class ServiciuDetailView(DetailView):
    model=Serviciu
    template_name ='details.html'
    context_object_name = 'service'
    pk_url_kwarg = 'id'

class ServiciuCreateView(LoginRequiredMixin, CreateView):
    model=Serviciu
    template_name='creates.html'
    fields = '__all__'
    success_url = reverse_lazy('services')

class ServiciuUpdateView(PermissionRequiredMixin,UpdateView):
    model = Serviciu
    permission_required=('pages.can_edit')
    template_name = 'updates.html'
    context_object_name = 'service'
    fields = '__all__'
    success_url = reverse_lazy('services')
    pk_url_kwarg = 'id'

class ServiciuDeleteView(PermissionRequiredMixin, DeleteView):
    model = Serviciu
    permission_required = ('pages.can_delete')
    template_name = 'deletes.html'
    context_object_name = 'service'
    fields = '__all__'
    success_url = reverse_lazy('services')
    pk_url_kwarg = 'id'


def thank_you(request):
    return HttpResponse('Form succesfully submitted')


def my_form(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            nume_prenume = form.cleaned_data['nume_prenume']
            email = form.cleaned_data['email']
            mesaj = form.cleaned_data['mesaj']
            try:
                send_mail(nume_prenume, mesaj, email, ['adina.alexandra13@yahoo.com'])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return redirect('thank_you')
    return render(request, "my_form.html", {'form': form})

def thank_you(request):
    return HttpResponse('Thank you for your message.')

def counter(request):
    visits_count = request.session.get('visits_count', 0)
    request.session['visits_count'] = visits_count + 1
    context = {
        'visits_count': visits_count,
    }

    return render(request, 'counter.html', context=context)