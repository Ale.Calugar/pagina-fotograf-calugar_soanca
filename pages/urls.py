from django.urls import path
from . import views
urlpatterns = [
    path('', views.home_page_view, name='home'),
    path('contact/', views.contact, name='contact'),
    path('aboutme/', views.aboutme, name='aboutme'),
    path('nunta/', views.nunta, name='nunta'),
    path('botez/', views.botez, name='botez'),
    path('majorat/', views.majorat, name='majorat'),
    path('sedinta/', views.sedinta, name='sedinta'),
    path('conditii/', views.conditii, name='conditii'),
    path('portofoliu/', views.portofoliu, name='portofoliu'),
    path('servicii/', views.AboutView.as_view(), name='servicii'),
    path('clienti/', views.ClientListView.as_view(), name='clienti'),
    path('clienti/<int:id>/', views.ClientDetailView.as_view(), name='detail'),
    path('create/', views.ClientCreateView.as_view(), name='create'),
    path('update/<int:id>/', views.ClientUpdateView.as_view(), name='update'),
    path('delete/<int:id>/', views.ClientDeleteView.as_view(), name='delete'),
    path('services/', views.ServiciuListView.as_view(), name='services'),
    path('services/<int:id>/', views.ServiciuDetailView.as_view(), name='details'),
    path('creates/', views.ServiciuCreateView.as_view(), name='creates'),
    path('updates/<int:id>/', views.ServiciuUpdateView.as_view(), name='updates'),
    path('deletes/<int:id>/', views.ServiciuDeleteView.as_view(), name='deletes'),
    path('my-form/', views.my_form, name='my_form'),
    path('thank-you/', views.thank_you, name='thank_you'),
    path('counter/', views.counter, name='counter')
    ]