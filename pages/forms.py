from django import forms

class ContactForm(forms.Form):
    nume_prenume = forms.CharField(max_length=300, required=True)
    email = forms.EmailField(required=True)
    mesaj = forms.CharField(widget=forms.Textarea)
