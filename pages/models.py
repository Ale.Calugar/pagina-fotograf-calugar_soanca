from django.db import models


class Serviciu(models.Model):
    nume=models.CharField(max_length=35,  default=0, blank=True, null=True)

    def __str__(self):
        return f"{self.nume}"

    class Meta:
        verbose_name = 'Serviciu'
        verbose_name_plural = 'Servicii'
        permissions = (
            ('can_edit', 'Can edit serviciu'),
            ('can_delete', 'Can delete serviciu')
        )


class Client(models.Model):
    nume_prenume=models.CharField(max_length=300)
    nrtelefon=models.CharField(max_length=10)
    data=models.DateField()
    locatia=models.CharField(max_length=300)
    serviciu=models.ForeignKey(Serviciu, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return f"{self.nume_prenume, self.serviciu}"

    class Meta:
        verbose_name = 'Client'
        verbose_name_plural = 'Clienti'
        ordering = ['data']
        permissions = (
            ('can_edit', 'Can edit client'),
            ('can_delete', 'Can delete client')
        )
